\begin{thebibliography}{}

\bibitem[Abragam, 1961]{Abragam61}
Abragam, A. (1961).
\newblock {\em The principles of nuclear magnetism}.
\newblock Clarendon Press, Oxford.

\bibitem[Akaike, 1973]{Akaike73}
Akaike, H. (1973).
\newblock Information theory and an extension of the maximum likelihood
  principle.
\newblock In: Petrov, B.~N. and Csaki, F. (eds.): {\em Proceedings of the
  {S}econd {I}nternational {S}ymposium on {I}nformation {T}heory}. Budapest,
  pages 267--281, Akademia Kiado.

\bibitem[Baldwin, 2014]{Baldwin2014}
Baldwin, A.~J. (2014).
\newblock An exact solution for ${R}_{2,\texttt{eff}}$ in {CPMG} experiments in
  the case of two site chemical exchange.
\newblock {\em J. Magn. Reson.}, {\bf 244}, 114--124.
\newblock
  (\href{http://dx.doi.org/10.1016/j.jmr.2014.02.023}{10.1016/j.jmr.2014.02.023}).

\bibitem[Baldwin and Kay, 2013]{Baldwin2013}
Baldwin, A.~J. and Kay, L.~E. (2013).
\newblock An {R}$_{1\rho}$ expression for a spin in chemical exchange between
  two sites with unequal transverse relaxation rates.
\newblock {\em J. Biomol. NMR}, {\bf 55}(2), 211--218.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-012-9694-6}{10.1007/s10858-012-9694-6}).

\bibitem[Barbato et~al., 1992]{Barbato92}
Barbato, G., Ikura, M., Kay, L.~E., Pastor, R.~W., and Bax, A. (1992).
\newblock Backbone dynamics of calmodulin studied by $^{15}${N} relaxation
  using inverse detected two-dimensional {NMR} spectroscopy: the central helix
  is flexible.
\newblock {\em Biochemistry}, {\bf 31}(23), 5269--5278.
\newblock (\href{http://dx.doi.org/10.1021/bi00138a005}{10.1021/bi00138a005}).

\bibitem[Bieri et~al., 2011]{Bieri11}
Bieri, M., d'Auvergne, E., and Gooley, P. (2011).
\newblock relax{GUI}: a new software for fast and simple {NMR} relaxation data
  analysis and calculation of ps-ns and $\mu$s motion of proteins.
\newblock {\em J. Biomol. NMR}, {\bf 50}, 147--155.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-011-9509-1}{10.1007/s10858-011-9509-1}).

\bibitem[Bieri and Gooley, 2011]{BieriGooley11}
Bieri, M. and Gooley, P.~R. (2011).
\newblock Automated {NMR} relaxation dispersion data analysis using {NESSY}.
\newblock {\em BMC Bioinformatics}, {\bf 12}, 421.
\newblock
  (\href{http://dx.doi.org/10.1186/1471-2105-12-421}{10.1186/1471-2105-12-421}).

\bibitem[Bloch, 1946]{Bloch46}
Bloch, F. (1946).
\newblock Nuclear induction.
\newblock {\em Phys. Rev.}, {\bf 70}(7-8), 460--474.
\newblock
  (\href{http://dx.doi.org/10.1103/PhysRev.70.460}{10.1103/PhysRev.70.460}).

\bibitem[Bloembergen et~al., 1948]{Bloembergen48}
Bloembergen, N., Purcell, E.~M., and Pound, R.~V. (1948).
\newblock Relaxation effects in nuclear magnetic resonance absorption.
\newblock {\em Phys. Rev.}, {\bf 73}(7), 679--712.
\newblock
  (\href{http://dx.doi.org/10.1103/PhysRev.73.679}{10.1103/PhysRev.73.679}).

\bibitem[Bonev and Gosselin, 2006]{BonevGosselin06}
Bonev, I.~A. and Gosselin, C.~M. (2006).
\newblock Analytical determination of the workspace of symmetrical spherical
  parallel mechanisms.
\newblock {\bf 22}(5), 1011--1017.
\newblock
  (\href{http://dx.doi.org/10.1109/TRO.2006.878983}{10.1109/TRO.2006.878983}).

\bibitem[Broyden, 1970]{Broyden70}
Broyden, C.~G. (1970).
\newblock The convergence of a class of double-rank minimization algorithms 1.
  {G}eneral considerations.
\newblock {\em J. Inst. Maths. Applics.}, {\bf 6}(1), 76--90.
\newblock
  (\href{http://dx.doi.org/10.1093/imamat/6.1.76}{10.1093/imamat/6.1.76}).

\bibitem[Br{\"u}schweiler et~al., 1995]{Bruschweiler95}
Br{\"u}schweiler, R., Liao, X., and Wright, P.~E. (1995).
\newblock Long-range motional restrictions in a multidomain zinc-finger protein
  from anisotropic tumbling.
\newblock {\em Science}, {\bf 268}(5212), 886--889.
\newblock
  (\href{http://dx.doi.org/10.1126/science.7754375}{10.1126/science.7754375}).

\bibitem[Butterwick et~al., 2004]{Butterwick04}
Butterwick, J.~A., Loria, P.~J., Astrof, N.~S., Kroenke, C.~D., Cole, R.,
  Rance, M., and Palmer, 3rd, A.~G. (2004).
\newblock Multiple time scale backbone dynamics of homologous thermophilic and
  mesophilic ribonuclease {HI} enzymes.
\newblock {\em J. Mol. Biol.}, {\bf 339}(4), 855--871.
\newblock
  (\href{http://dx.doi.org/10.1016/j.jmb.2004.03.055}{10.1016/j.jmb.2004.03.055}).

\bibitem[Carver and Richards, 1972]{CarverRichards72}
Carver, J.~P. and Richards, R.~E. (1972).
\newblock General 2-site solution for chemical exchange produced dependence of
  ${T_2}$ upon {C}arr-{P}urcell pulse separation.
\newblock {\em J. Magn. Reson.}, {\bf 6}(1), 89--105.
\newblock
  (\href{http://dx.doi.org/10.1016/0022-2364(72)90090-X}{10.1016/0022-2364(72)90090-X}).

\bibitem[Chen et~al., 2004]{Chen04}
Chen, J., Brooks, 3rd, C.~L., and Wright, P.~E. (2004).
\newblock Model-free analysis of protein dynamics: assessment of accuracy and
  model selection protocols based on molecular dynamics simulation.
\newblock {\em J. Biomol. NMR}, {\bf 29}(3), 243--257.
\newblock
  (\href{http://dx.doi.org/10.1023/b:jnmr.0000032504.70912.58}{10.1023/b:jnmr.0000032504.70912.58}).

\bibitem[Clore et~al., 1990]{Clore90a}
Clore, G.~M., Szabo, A., Bax, A., Kay, L.~E., Driscoll, P.~C., and Gronenborn,
  A.~M. (1990).
\newblock Deviations from the simple 2-parameter model-free approach to the
  interpretation of $^{15}${N} nuclear magnetic-relaxation of proteins.
\newblock {\em J. Am. Chem. Soc.}, {\bf 112}(12), 4989--4991.
\newblock (\href{http://dx.doi.org/10.1021/ja00168a070}{10.1021/ja00168a070}).

\bibitem[Crawford et~al., 1999]{Crawford99}
Crawford, N.~R., Yamaguchi, G.~T., and Dickman, C.~A. (1999).
\newblock A new technique for determining 3-{D} joint angles: the tilt/twist
  method.
\newblock {\em Clin. Biomech. (Bristol, Avon)}, {\bf 14}(3), 153--165.
\newblock
  (\href{http://dx.doi.org/10.1016/S0268-0033(98)00080-1}{10.1016/S0268-0033(98)00080-1}).

\bibitem[d'Auvergne, 2006]{dAuvergne06}
d'Auvergne, E.~J. (2006).
\newblock {\em Protein dynamics: a study of the model-free analysis of {NMR}
  relaxation data.}
\newblock PhD thesis, Biochemistry and Molecular Biology, University of
  Melbourne. http://eprints.infodiv.unimelb.edu.au/archive/00002799/.
\newblock (\href{http://dx.doi.org/10187/2281}{10187/2281}).

\bibitem[d'Auvergne and Gooley, 2003]{dAuvergneGooley03}
d'Auvergne, E.~J. and Gooley, P.~R. (2003).
\newblock The use of model selection in the model-free analysis of protein
  dynamics.
\newblock {\em J. Biomol. NMR}, {\bf 25}(1), 25--39.
\newblock
  (\href{http://dx.doi.org/10.1023/a:1021902006114}{10.1023/a:1021902006114}).

\bibitem[d'Auvergne and Gooley, 2006]{dAuvergneGooley06}
d'Auvergne, E.~J. and Gooley, P.~R. (2006).
\newblock Model-free model elimination: {A} new step in the model-free dynamic
  analysis of {NMR} relaxation data.
\newblock {\em J. Biomol. NMR}, {\bf 35}(2), 117--135.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-006-9007-z}{10.1007/s10858-006-9007-z}).

\bibitem[d'Auvergne and Gooley, 2007]{dAuvergneGooley07}
d'Auvergne, E.~J. and Gooley, P.~R. (2007).
\newblock Set theory formulation of the model-free problem and the diffusion
  seeded model-free paradigm.
\newblock {\em Mol. BioSyst.}, {\bf 3}(7), 483--494.
\newblock (\href{http://dx.doi.org/10.1039/b702202f}{10.1039/b702202f}).

\bibitem[d'Auvergne and Gooley, 2008a]{dAuvergneGooley08ab}
d'Auvergne, E.~J. and Gooley, P.~R. (2008a).
\newblock Optimisation of {NMR} dynamic models.
\newblock {\em J. Biomol. NMR}, {\bf 40}(2), 107--133.

\bibitem[d'Auvergne and Gooley, 2008b]{dAuvergneGooley08a}
d'Auvergne, E.~J. and Gooley, P.~R. (2008b).
\newblock Optimisation of {NMR} dynamic models {I}. {M}inimisation algorithms
  and their performance within the model-free and {B}rownian rotational
  diffusion spaces.
\newblock {\em J. Biomol. NMR}, {\bf 40}(2), 107--119.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-007-9214-2}{10.1007/s10858-007-9214-2}).

\bibitem[d'Auvergne and Gooley, 2008c]{dAuvergneGooley08b}
d'Auvergne, E.~J. and Gooley, P.~R. (2008c).
\newblock Optimisation of {NMR} dynamic models {II}. {A} new methodology for
  the dual optimisation of the model-free parameters and the {B}rownian
  rotational diffusion tensor.
\newblock {\em J. Biomol. NMR}, {\bf 40}(2), 121--133.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-007-9213-3}{10.1007/s10858-007-9213-3}).

\bibitem[d'Auvergne and Griesinger, 2019]{dAuvergneGriesinger19}
d'Auvergne, E.~J. and Griesinger, C. (2019).
\newblock The theory of frame ordering: observing motions in calmodulin
  complexes.
\newblock {\em Q. Rev. Biophys.}, {\bf 52}, e3.
\newblock
  (\href{http://dx.doi.org/10.1017/S0033583519000015}{10.1017/S0033583519000015}).

\bibitem[Davis et~al., 1994]{Davis94}
Davis, D.~G., Perlman, M.~E., and London, R.~E. (1994).
\newblock Direct measurements of the dissociation-rate constant for
  inhibitor-enzyme complexes via the ${T}_{1\rho}$ and ${T}_2$ ({CPMG})
  methods.
\newblock {\em J. Magn. Reson.}, {\bf 104}(3), 266--275.
\newblock
  (\href{http://dx.doi.org/10.1006/jmrb.1994.1084}{10.1006/jmrb.1994.1084}).

\bibitem[Einstein, 1905]{Einstein05}
Einstein, A. (1905).
\newblock {\"U}ber die von der molekularkinetischen {T}heorie der {W}\"arme
  geforderte {B}ewegung von in ruhenden {F}l\"ussigkeiten suspendierten
  {T}eilchen [{T}he motion of elements suspended in static liquids as claimed
  in the molecular kinetic theory of heat].
\newblock {\em Ann. Physik}, {\bf 17}(8), 549--560.
\newblock
  (\href{http://dx.doi.org/10.1002/andp.19053220806}{10.1002/andp.19053220806}).

\bibitem[Erdelyi et~al., 2011]{Erdelyi11}
Erdelyi, M., d'Auvergne, E., Navarro-Vazquez, A., Leonov, A., and Griesinger,
  C. (2011).
\newblock Dynamics of the glycosidic bond: conformational space of lactose.
\newblock {\em Chem. Eur. J.}, {\bf 17}(34), 9368--9376.
\newblock
  (\href{http://dx.doi.org/10.1002/chem.201100854}{10.1002/chem.201100854}).

\bibitem[Farrow et~al., 1995]{Farrow95}
Farrow, N.~A., Zhang, O.~W., Szabo, A., Torchia, D.~A., and Kay, L.~E. (1995).
\newblock Spectral density-function mapping using $^{15}${N} relaxation data
  exclusively.
\newblock {\em J. Biomol. NMR}, {\bf 6}(2), 153--162.
\newblock (\href{http://dx.doi.org/10.1007/bf00211779}{10.1007/bf00211779}).

\bibitem[Favro, 1960]{Favro60}
Favro, L.~D. (1960).
\newblock Theory of the rotational {B}rownian motion of a free rigid body.
\newblock {\em Phys. Rev.}, {\bf 119}(1), 53--62.
\newblock
  (\href{http://dx.doi.org/10.1103/PhysRev.119.53}{10.1103/PhysRev.119.53}).

\bibitem[Fletcher, 1970]{Fletcher70}
Fletcher, R. (1970).
\newblock A new approach to variable metric algorithms.
\newblock {\em Comp. J.}, {\bf 13}(3), 317--322.
\newblock
  (\href{http://dx.doi.org/10.1093/comjnl/13.3.317}{10.1093/comjnl/13.3.317}).

\bibitem[Fletcher and Reeves, 1964]{FletcherReeves64}
Fletcher, R. and Reeves, C.~M. (1964).
\newblock Function minimization by conjugate gradients.
\newblock {\em Comp. J.}, {\bf 7}(2), 149--154.
\newblock
  (\href{http://dx.doi.org/10.1093/comjnl/7.2.149}{10.1093/comjnl/7.2.149}).

\bibitem[Fushman et~al., 1997]{Fushman97}
Fushman, D., Cahill, S., and Cowburn, D. (1997).
\newblock The main-chain dynamics of the dynamin pleckstrin homology ({PH})
  domain in solution: analysis of $^{15}${N} relaxation with monomer/dimer
  equilibration.
\newblock {\em J. Mol. Biol.}, {\bf 266}(1), 173--194.
\newblock
  (\href{http://dx.doi.org/10.1006/jmbi.1996.0771}{10.1006/jmbi.1996.0771}).

\bibitem[Fushman et~al., 1998]{Fushman98}
Fushman, D., Tjandra, N., and Cowburn, D. (1998).
\newblock Direct measurement of $^{15}${N} chemical shift anisotropy in
  solution.
\newblock {\em J. Am. Chem. Soc.}, {\bf 120}(42), 10947--10952.
\newblock (\href{http://dx.doi.org/10.1021/ja981686m}{10.1021/ja981686m}).

\bibitem[Fushman et~al., 1999]{Fushman99}
Fushman, D., Tjandra, N., and Cowburn, D. (1999).
\newblock An approach to direct determination of protein dynamics from
  $^{15}${N} {NMR} relaxation at multiple fields, independent of variable
  $^{15}${N} chemical shift anisotropy and chemical exchange contributions.
\newblock {\em J. Am. Chem. Soc.}, {\bf 121}(37), 8577--8582.
\newblock (\href{http://dx.doi.org/10.1021/ja9904991}{10.1021/ja9904991}).

\bibitem[Gill et~al., 1981]{GMW81}
Gill, P.~E., Murray, W., and Wright, M.~H. (1981).
\newblock {\em Practical optimization}.
\newblock Academic Press.

\bibitem[Goldfarb, 1970]{Goldfarb70}
Goldfarb, D. (1970).
\newblock A family of variable-metric methods derived by variational means.
\newblock {\em Math. Comp.}, {\bf 24}(109), 23--26.
\newblock
  (\href{http://dx.doi.org/10.1090/s0025-5718-1970-0258249-6}{10.1090/s0025-5718-1970-0258249-6}).

\bibitem[Hansen et~al., 2008]{Hansen08}
Hansen, D.~F., Vallurupalli, P., Lundstrom, P., Neudecker, P., and Kay, L.~E.
  (2008).
\newblock Probing chemical shifts of invisible states of proteins with
  relaxation dispersion {NMR} spectroscopy: how well can we do?
\newblock {\em J. Am. Chem. Soc.}, {\bf 130}(8), 2667--2675.
\newblock (\href{http://dx.doi.org/10.1021/ja078337p}{10.1021/ja078337p}).

\bibitem[Hestenes and Stiefel, 1952]{HestenesStiefel52}
Hestenes, M.~R. and Stiefel, E. (1952).
\newblock Methods of conjugate gradients for solving linear systems.
\newblock {\em J. Res. Natn. Bur. Stand.}, {\bf 49}(6), 409--436.
\newblock
  (\href{http://dx.doi.org/10.6028/jres.049.044}{10.6028/jres.049.044}).

\bibitem[Horne et~al., 2007]{Horne07}
Horne, J., d'Auvergne, E.~J., Coles, M., Velkov, T., Chin, Y., Charman, W.~N.,
  Prankerd, R., Gooley, P.~R., and Scanlon, M.~J. (2007).
\newblock Probing the flexibility of the {D}sb{A} oxidoreductase from
  \textit{{V}ibrio cholerae}--a $^{15}${N} - $^1${H} heteronuclear {NMR}
  relaxation analysis of oxidized and reduced forms of {D}sb{A}.
\newblock {\em J. Mol. Biol.}, {\bf 371}(3), 703--716.
\newblock
  (\href{http://dx.doi.org/10.1016/j.jmb.2007.05.067}{10.1016/j.jmb.2007.05.067}).

\bibitem[Huang et~al., 1999]{Huang99}
Huang, T., Wang, J., and Whitehouse, D.~J. (1999).
\newblock Closed form solution to workspace of hexapod-based virtual axis
  machine tools.
\newblock {\em J. Mech. Des.}, {\bf 121}(1), 26--31.
\newblock (\href{http://dx.doi.org/10.1115/1.2829424}{10.1115/1.2829424}).

\bibitem[Hurvich and Tsai, 1989]{HurvichTsai89}
Hurvich, C.~M. and Tsai, C.~L. (1989).
\newblock Regression and time-series model selection in small samples.
\newblock {\em Biometrika}, {\bf 76}(2), 297--307.
\newblock
  (\href{http://dx.doi.org/10.1093/biomet/76.2.297}{10.1093/biomet/76.2.297}).

\bibitem[Ishima and Torchia, 1999]{IshimaTorchia99}
Ishima, R. and Torchia, D.~A. (1999).
\newblock Estimating the time scale of chemical exchange of proteins from
  measurements of transverse relaxation rates in solution.
\newblock {\em J. Biomol. NMR}, {\bf 14}(4), 369--372.
\newblock
  (\href{http://dx.doi.org/10.1023/A:1008324025406}{10.1023/A:1008324025406}).

\bibitem[Ishima and Torchia, 2005]{IshimaTorchia05}
Ishima, R. and Torchia, D.~A. (2005).
\newblock Error estimation and global fitting in transverse-relaxation
  dispersion experiments to determine chemical-exchange parameters.
\newblock {\em J. Biomol. NMR}, {\bf 32}(1), 41--54.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-005-3593-z}{10.1007/s10858-005-3593-z}).

\bibitem[Kay et~al., 1989]{Kay89}
Kay, L.~E., Torchia, D.~A., and Bax, A. (1989).
\newblock Backbone dynamics of proteins as studied by $^{15}${N} inverse
  detected heteronuclear {NMR} spectroscopy: application to staphylococcal
  nuclease.
\newblock {\em Biochemistry}, {\bf 28}(23), 8972--8979.
\newblock (\href{http://dx.doi.org/10.1021/bi00449a003}{10.1021/bi00449a003}).

\bibitem[Kleckner and Foster, 2012]{KlecknerFoster12}
Kleckner, I.~R. and Foster, M.~P. (2012).
\newblock G{UARDD}: user-friendly {MATLAB} software for rigorous analysis of
  {CPMG} {RD} {NMR} data.
\newblock {\em J. Biomol. NMR}, {\bf 52}(1), 11--22.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-011-9589-y}{10.1007/s10858-011-9589-y}).

\bibitem[Korein, 1985]{Korein85}
Korein, J.~U. (1985).
\newblock {\em A geometric investigation of reach}.
\newblock MIT Press, Cambridge, MA, USA.

\bibitem[Korzhnev et~al., 2001]{Korzhnev01}
Korzhnev, D.~M., Billeter, M., Arseniev, A.~S., and Orekhov, V.~Y. (2001).
\newblock {NMR} studies of {B}rownian tumbling and internal motions in
  proteins.
\newblock {\em Prog. NMR Spectrosc.}, {\bf 38}(3), 197--266.
\newblock
  (\href{http://dx.doi.org/10.1016/s0079-6565(00)00028-5}{10.1016/s0079-6565(00)00028-5}).

\bibitem[Korzhnev et~al., 2004a]{Korzhnev04a}
Korzhnev, D.~M., Kloiber, K., Kanelis, V., Tugarinov, V., and Kay, L.~E.
  (2004a).
\newblock Probing slow dynamics in high molecular weight proteins by
  methyl-{TROSY} {NMR} spectroscopy: application to a 723-residue enzyme.
\newblock {\em J. Am. Chem. Soc.}, {\bf 126}(12), 3964--3973.
\newblock (\href{http://dx.doi.org/10.1021/ja039587i}{10.1021/ja039587i}).

\bibitem[Korzhnev et~al., 2004b]{Korzhnev04b}
Korzhnev, D.~M., Kloiber, K., and Kay, L.~E. (2004b).
\newblock Multiple-quantum relaxation dispersion {NMR} spectroscopy probing
  millisecond time-scale dynamics in proteins: theory and application.
\newblock {\em J. Am. Chem. Soc.}, {\bf 126}(23), 7320--7329.
\newblock (\href{http://dx.doi.org/10.1021/ja049968b}{10.1021/ja049968b}).

\bibitem[Korzhnev et~al., 2005a]{Korzhnev05b}
Korzhnev, D.~M., Neudecker, P., Mittermaier, A., Orekhov, V.~Y., and Kay, L.~E.
  (2005a).
\newblock Multiple-site exchange in proteins studied with a suite of six {NMR}
  relaxation dispersion experiments: an application to the folding of a {F}yn
  {SH}3 domain mutant.
\newblock {\em J. Am. Chem. Soc.}, {\bf 127}(44), 15602--15611.
\newblock (\href{http://dx.doi.org/10.1021/ja054550e}{10.1021/ja054550e}).

\bibitem[Korzhnev et~al., 2005b]{Korzhnev05a}
Korzhnev, D.~M., Orekhov, V.~Y., and Kay, L.~E. (2005b).
\newblock Off-resonance {R}$_{1\rho}$ {NMR} studies of exchange dynamics in
  proteins with low spin-lock fields: an application to a {F}yn {SH}3 domain.
\newblock {\em J. Am. Chem. Soc.}, {\bf 127}(2), 713--721.
\newblock (\href{http://dx.doi.org/10.1021/ja0446855}{10.1021/ja0446855}).

\bibitem[Kullback and Leibler, 1951]{KullbackLeibler51}
Kullback, S. and Leibler, R.~A. (1951).
\newblock On information and sufficiency.
\newblock {\em Ann. Math. Stat.}, {\bf 22}(1), 79--86.
\newblock
  (\href{http://dx.doi.org/10.1214/aoms/1177729694}{10.1214/aoms/1177729694}).

\bibitem[Lee et~al., 1997]{Lee97}
Lee, L.~K., Rance, M., Chazin, W.~J., and Palmer, A.~G. (1997).
\newblock Rotational diffusion anisotropy of proteins from simultaneous
  analysis of $^{15}${N} and $^{13}${C}$^\alpha$ nuclear spin relaxation.
\newblock {\em J. Biomol. NMR}, {\bf 9}(3), 287--298.
\newblock
  (\href{http://dx.doi.org/10.1023/a:1018631009583}{10.1023/a:1018631009583}).

\bibitem[Lefevre et~al., 1996]{Lefevre96}
Lefevre, J.~F., Dayie, K.~T., Peng, J.~W., and Wagner, G. (1996).
\newblock Internal mobility in the partially folded {DNA} binding and
  dimerization domains of {GAL}4: {NMR} analysis of the {N}-{H} spectral
  density functions.
\newblock {\em Biochemistry}, {\bf 35}(8), 2674--2686.
\newblock (\href{http://dx.doi.org/10.1021/bi9526802}{10.1021/bi9526802}).

\bibitem[Levenberg, 1944]{Levenberg44}
Levenberg, K. (1944).
\newblock A method for the solution of certain non-linear problems in least
  squares.
\newblock {\em Quarterly of Applied Mathematics}, {\bf 2}, 164--168.

\bibitem[Linhart and Zucchini, 1986]{LinhartZucchini86}
Linhart, H. and Zucchini, W. (1986).
\newblock {\em Model selection}.
\newblock Wiley Series in Probability and Mathematical Statistics. John Wiley
  \& Sons, Inc., New York, NY, USA.

\bibitem[Lipari and Szabo, 1982a]{LipariSzabo82a}
Lipari, G. and Szabo, A. (1982a).
\newblock Model-free approach to the interpretation of nuclear
  magnetic-resonance relaxation in macromolecules {I}. {T}heory and range of
  validity.
\newblock {\em J. Am. Chem. Soc.}, {\bf 104}(17), 4546--4559.
\newblock (\href{http://dx.doi.org/10.1021/ja00381a009}{10.1021/ja00381a009}).

\bibitem[Lipari and Szabo, 1982b]{LipariSzabo82b}
Lipari, G. and Szabo, A. (1982b).
\newblock Model-free approach to the interpretation of nuclear
  magnetic-resonance relaxation in macromolecules {II}. {A}nalysis of
  experimental results.
\newblock {\em J. Am. Chem. Soc.}, {\bf 104}(17), 4559--4570.
\newblock (\href{http://dx.doi.org/10.1021/ja00381a010}{10.1021/ja00381a010}).

\bibitem[Luz and Meiboom, 1963]{LuzMeiboom63}
Luz, Z. and Meiboom, S. (1963).
\newblock Nuclear magnetic resonance study of protolysis of trimethylammonium
  ion in aqueous solution - order of reaction with respect to solvent.
\newblock {\em J. Chem. Phys.}, {\bf 39}(2), 366--370.
\newblock (\href{http://dx.doi.org/10.1063/1.1734254}{10.1063/1.1734254}).

\bibitem[Mandel et~al., 1995]{Mandel95}
Mandel, A.~M., Akke, M., and Palmer, 3rd, A.~G. (1995).
\newblock Backbone dynamics of \textit{{E}scherichia coli} ribonuclease {HI}:
  correlations with structure and function in an active enzyme.
\newblock {\em J. Mol. Biol.}, {\bf 246}(1), 144--163.
\newblock
  (\href{http://dx.doi.org/10.1006/jmbi.1994.0073}{10.1006/jmbi.1994.0073}).

\bibitem[Marquardt, 1963]{Marquardt63}
Marquardt, D.~W. (1963).
\newblock An algorithm for least squares estimation of non-linear parameters.
\newblock {\em SIAM J.}, {\bf 11}, 431--441.
\newblock (\href{http://dx.doi.org/10.1137/0111030}{10.1137/0111030}).

\bibitem[Mazur et~al., 2013]{Mazur13}
Mazur, A., Hammesfahr, B., Griesinger, C., Lee, D., and Kollmar, M. (2013).
\newblock Shere{K}han--calculating exchange parameters in relaxation dispersion
  data from {CPMG} experiments.
\newblock {\em Bioinformatics}, {\bf 29}(14), 1819--1820.
\newblock
  (\href{http://dx.doi.org/10.1093/bioinformatics/btt286}{10.1093/bioinformatics/btt286}).

\bibitem[McConnell, 1958]{McConnell58}
McConnell, H.~M. (1958).
\newblock Reaction rates by nuclear magnetic resonance.
\newblock {\em J. Chem. Phys.}, {\bf 28}(3), 430--431.
\newblock (\href{http://dx.doi.org/10.1063/1.1744152}{10.1063/1.1744152}).

\bibitem[Meiboom, 1961]{Meiboom61}
Meiboom, S. (1961).
\newblock Nuclear magnetic resonance study of proton transfer in water.
\newblock {\em J. Chem. Phys.}, {\bf 34}(2), 375--388.
\newblock (\href{http://dx.doi.org/10.1063/1.1700960}{10.1063/1.1700960}).

\bibitem[Millet et~al., 2000]{Millet00}
Millet, O., Loria, J.~P., Kroenke, C.~D., Pons, M., and Palmer, A.~G. (2000).
\newblock The static magnetic field dependence of chemical exchange
  linebroadening defines the {NMR} chemical shift time scale.
\newblock {\em J. Am. Chem. Soc.}, {\bf 122}(12), 2867--2877.

\bibitem[Miloushev and Palmer, 2005]{MiloushevPalmer05}
Miloushev, V.~Z. and Palmer, 3rd, A.~G. (2005).
\newblock R$_{1\rho}$ relaxation for two-site chemical exchange: general
  approximations and some exact solutions.
\newblock {\em J. Magn. Reson.}, {\bf 177}(2), 221--227.
\newblock
  (\href{http://dx.doi.org/10.1016/j.jmr.2005.07.023}{10.1016/j.jmr.2005.07.023}).

\bibitem[Mor{\'e} and Thuente, 1994]{MoreThuente94}
Mor{\'e}, J.~J. and Thuente, D.~J. (1994).
\newblock Line search algorithms with guaranteed sufficient decrease.
\newblock {\em ACM Trans. Maths. Softw.}, {\bf 20}(3), 286--307.
\newblock
  (\href{http://dx.doi.org/10.1145/192115.192132}{10.1145/192115.192132}).

\bibitem[Morin, 2011]{Morin11}
Morin, S. (2011).
\newblock A practical guide to protein dynamics from $^{15}${N} spin relaxation
  in solution.
\newblock {\em Prog. NMR Spectrosc.}, {\bf 59}(3), 245--262.
\newblock
  (\href{http://dx.doi.org/10.1016/j.pnmrs.2010.12.003}{10.1016/j.pnmrs.2010.12.003}).

\bibitem[Morin and Gagn{\'e}, 2009a]{MorinGagne09a}
Morin, S. and Gagn{\'e}, S. (2009a).
\newblock Simple tests for the validation of multiple field spin relaxation
  data.
\newblock {\em J. Biomol. NMR}, {\bf 45}, 361--372.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-009-9381-4}{10.1007/s10858-009-9381-4}).

\bibitem[Morin and Gagn{\'e}, 2009b]{MorinGagne09b}
Morin, S. and Gagn{\'e}, S.~M. (2009b).
\newblock {NMR} dynamics of {PSE}-4 $\beta$-lactamase: An interplay of ps-ns
  order and $\mu$s-ms motions in the active site.
\newblock {\em Biophys. J.}, {\bf 96}(11), 4681--4691.
\newblock
  (\href{http://dx.doi.org/10.1016/j.bpj.2009.02.068}{10.1016/j.bpj.2009.02.068}).

\bibitem[Morin et~al., 2014]{Morin14}
Morin, S., Linnet, T.~E., Lescanne, M., Schanda, P., Thompson, G.~S.,
  Tollinger, M., Teilum, K., Gagne, S., Marion, D., Griesinger, C., Blackledge,
  M., and d'Auvergne, E.~J. (2014).
\newblock {r}elax: the analysis of biomolecular kinetics and thermodynamics
  using {NMR} relaxation dispersion data.
\newblock {\em Bioinformatics}, {\bf 30}(15), 2219--2220.
\newblock
  (\href{http://dx.doi.org/10.1093/bioinformatics/btu166}{10.1093/bioinformatics/btu166}).

\bibitem[Nocedal and Wright, 1999]{NocedalWright99}
Nocedal, J. and Wright, S.~J. (1999).
\newblock {\em Numerical Optimization}.
\newblock Springer Series in Operations Research. Springer-Verlag, New York.

\bibitem[O'Connell et~al., 2009]{OConnell09}
O'Connell, N.~E., Grey, M.~J., Tang, Y., Kosuri, P., Miloushev, V.~Z., Raleigh,
  D.~P., and Palmer, 3rd, A.~G. (2009).
\newblock Partially folded equilibrium intermediate of the villin headpiece
  {HP}67 defined by $^{13}${C} relaxation dispersion.
\newblock {\em J. Biomol. NMR}, {\bf 45}(1-2), 85--98.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-009-9340-0}{10.1007/s10858-009-9340-0}).

\bibitem[Orekhov et~al., 1999a]{Orekhov99b}
Orekhov, V.~Y., Korzhnev, D.~M., Diercks, T., Kessler, H., and Arseniev, A.~S.
  (1999a).
\newblock $^1${H}-$^{15}${N} {NMR} dynamic study of an isolated
  $\alpha$-helical peptide (1-36)bacteriorhodopsin reveals the equilibrium
  helix-coil transitions.
\newblock {\em J. Biomol. NMR}, {\bf 14}(4), 345--356.
\newblock
  (\href{http://dx.doi.org/10.1023/a:1008356809071}{10.1023/a:1008356809071}).

\bibitem[Orekhov et~al., 1999b]{Orekhov99a}
Orekhov, V.~Y., Korzhnev, D.~M., Pervushin, K.~V., Hoffmann, E., and Arseniev,
  A.~S. (1999b).
\newblock Sampling of protein dynamics in nanosecond time scale by $^{15}${N}
  {NMR} relaxation and self-diffusion measurements.
\newblock {\em J. Biomol. Struct. Dyn.}, {\bf 17}(1), 157--174.
\newblock
  (\href{http://dx.doi.org/10.1080/07391102.1999.10508348}{10.1080/07391102.1999.10508348}).

\bibitem[Orekhov et~al., 1995]{Orekhov95b}
Orekhov, V.~Y., Pervushin, K.~V., Korzhnev, D.~M., and Arseniev, A.~S. (1995).
\newblock Backbone dynamics of (1-71)bacterioopsin and (1-36)bacterioopsin
  studied by 2-dimensional $^1${H}-$^{15}${N} {NMR}-spectroscopy.
\newblock {\em J. Biomol. NMR}, {\bf 6}(2), 113--122.
\newblock (\href{http://dx.doi.org/10.1007/BF00211774}{10.1007/BF00211774}).

\bibitem[Palmer and Massi, 2006]{PalmerMassi06}
Palmer, 3rd, A.~G. and Massi, F. (2006).
\newblock Characterization of the dynamics of biomacromolecules using
  rotating-frame spin relaxation {NMR} spectroscopy.
\newblock {\em Chem. Rev.}, {\bf 106}(5), 1700--1719.
\newblock (\href{http://dx.doi.org/10.1021/cr0404287}{10.1021/cr0404287}).

\bibitem[Perrin, 1934]{Perrin34}
Perrin, F. (1934).
\newblock Mouvement {B}rownien d'un ellipso\"ide ({I}). {D}ispersion
  di\'eletrique pour des mol\'ecules ellipso\"idales [{B}rownian motion of an
  ellipsoid. ({I}) {D}ielectric dispersion for ellipsoidal molecules].
\newblock {\em J. Phys. Radium}, {\bf 5}, 497--511.
\newblock
  (\href{http://dx.doi.org/10.1051/jphysrad:01934005010049700}{10.1051/jphysrad:01934005010049700}).

\bibitem[Perrin, 1936]{Perrin36}
Perrin, F. (1936).
\newblock Mouvement {B}rownien d'un ellipso\"ide ({II}). {R}otation libre et
  d\'epolarisation des fluorescences. {T}ranslation et diffusion de mol\'ecules
  ellipso\"idales [{B}rownian motion of an ellipsoid ({II}). {F}ree rotation
  and fluorescence depolarisation. {T}ranslation and diffusion of ellipsoidal
  molecules].
\newblock {\em J. Phys. Radium}, {\bf 7}, 1--11.
\newblock
  (\href{http://dx.doi.org/10.1051/jphysrad:01936007010100}{10.1051/jphysrad:01936007010100}).

\bibitem[Polak and Ribi{\`e}re, 1969]{PolakRibiere69}
Polak, E. and Ribi{\`e}re, G. (1969).
\newblock Note sur la convergence de m{\'e}thodes de directions conjugu{\'e}es.
\newblock {\em Revue Fran{\c c}aise d'Informatique et de Recherche
  Op{\'e}rationnelle}, {\bf 16}, 35--43.

\bibitem[Schurr et~al., 1994]{Schurr94}
Schurr, J.~M., Babcock, H.~P., and Fujimoto, B.~S. (1994).
\newblock A test of the model-free formulas. {E}ffects of anisotropic
  rotational diffusion and dimerization.
\newblock {\em J. Magn. Reson. B}, {\bf 105}(3), 211--224.
\newblock
  (\href{http://dx.doi.org/10.1006/jmrb.1994.1127}{10.1006/jmrb.1994.1127}).

\bibitem[Schwarz, 1978]{Schwarz78}
Schwarz, G. (1978).
\newblock Estimating dimension of a model.
\newblock {\em Ann. Stat.}, {\bf 6}(2), 461--464.
\newblock
  (\href{http://dx.doi.org/10.1214/aos/1176344136}{10.1214/aos/1176344136}).

\bibitem[Shanno, 1970]{Shanno70}
Shanno, D.~F. (1970).
\newblock Conditioning of quasi-{N}ewton methods for function minimization.
\newblock {\em Math. Comp.}, {\bf 24}(111), 647--656.
\newblock
  (\href{http://dx.doi.org/10.1090/s0025-5718-1970-0274029-x}{10.1090/s0025-5718-1970-0274029-x}).

\bibitem[Sobol', 1967]{Sobol67}
Sobol', I. (1967).
\newblock Point distribution in a cube and approximate evaluation of integrals.
\newblock {\em Zhurnal Vychislitel'noi Matematiki i Matematicheskoi Fiziki},
  {\bf 7}, 784--802.
\newblock
  (\href{http://dx.doi.org/10.1016/0041-5553(67)90144-9}{10.1016/0041-5553(67)90144-9}).

\bibitem[Spencer, 1980]{Spencer80}
Spencer, A. J.~M. (1980).
\newblock {\em Continuum mechanics}.
\newblock Longman Group UK Limited, Essex, England.

\bibitem[Steihaug, 1983]{Steihaug83}
Steihaug, T. (1983).
\newblock The conjugate gradient method and trust regions in large scale
  optimization.
\newblock {\em SIAM J. Numer. Anal.}, {\bf 20}(3), 626--637.
\newblock (\href{http://dx.doi.org/10.1137/0720042}{10.1137/0720042}).

\bibitem[Sugase et~al., 2013]{Sugase13}
Sugase, K., Konuma, T., Lansing, J.~C., and Wright, P.~E. (2013).
\newblock Fast and accurate fitting of relaxation dispersion data using the
  flexible software package {GLOVE}.
\newblock {\em J. Biomol. NMR}, {\bf 56}(3), 275--283.
\newblock
  (\href{http://dx.doi.org/10.1007/s10858-013-9747-5}{10.1007/s10858-013-9747-5}).

\bibitem[Sun et~al., 2011]{Sun11}
Sun, H., d'Auvergne, E.~J., Reinscheid, U.~M., Dias, L.~C., Andrade, C. K.~Z.,
  Rocha, R.~O., and Griesinger, C. (2011).
\newblock Bijvoet in solution reveals unexpected stereoselectivity in a michael
  addition.
\newblock {\em Chem. Eur. J.}, {\bf 17}(6), 1811--1817.
\newblock
  (\href{http://dx.doi.org/10.1002/chem.201002520}{10.1002/chem.201002520}).

\bibitem[Tjandra et~al., 1996]{Tjandra96}
Tjandra, N., Wingfield, P., Stahl, S., and Bax, A. (1996).
\newblock Anisotropic rotational diffusion of perdeuterated {HIV} protease from
  $^{15}${N} {NMR} relaxation measurements at two magnetic.
\newblock {\em J. Biomol. NMR}, {\bf 8}(3), 273--284.
\newblock (\href{http://dx.doi.org/10.1007/bf00410326}{10.1007/bf00410326}).

\bibitem[Tollinger et~al., 2001]{Tollinger01}
Tollinger, M., Skrynnikov, N.~R., Mulder, F. A.~A., Forman-Kay, J.~D., and Kay,
  L.~E. (2001).
\newblock Slow dynamics in folded and unfolded states of an {SH3} domain.
\newblock {\em J. Am. Chem. Soc.}, {\bf 123}(46), 11341--11352.
\newblock (\href{http://dx.doi.org/10.1021/ja011300z}{10.1021/ja011300z}).

\bibitem[Trott et~al., 2003]{Trott03}
Trott, O., Abergel, D., and Palmer, A. (2003).
\newblock An average-magnetization analysis of {R}$_{1\rho}$ relaxation outside
  of the fast exchange.
\newblock {\em Mol. Phys.}, {\bf 101}(6), 753--763.
\newblock
  (\href{http://dx.doi.org/10.1080/0026897021000054826}{10.1080/0026897021000054826}).

\bibitem[Trott and Palmer, 2002]{TrottPalmer02}
Trott, O. and Palmer, 3rd, A.~G. (2002).
\newblock {R}$_{1\rho}$ relaxation outside of the fast-exchange limit.
\newblock {\em J. Magn. Reson.}, {\bf 154}(1), 157--160.
\newblock
  (\href{http://dx.doi.org/10.1006/jmre.2001.2466}{10.1006/jmre.2001.2466}).

\bibitem[Trott and Palmer, 2004]{TrottPalmer04}
Trott, O. and Palmer, 3rd, A.~G. (2004).
\newblock Theoretical study of {R}$_{1\rho}$ rotating-frame and {R}$_2$
  free-precession relaxation in the presence of \textit{n}-site chemical
  exchange.
\newblock {\em J. Magn. Reson.}, {\bf 170}(1), 104--112.
\newblock
  (\href{http://dx.doi.org/10.1016/j.jmr.2004.06.005}{10.1016/j.jmr.2004.06.005}).

\bibitem[Viles et~al., 2001]{Viles01}
Viles, J., Duggan, B., Zaborowski, E., Schwarzinger, S., Huntley, J., Kroon,
  G., Dyson, H.~J., and Wright, P. (2001).
\newblock Potential bias in {NMR} relaxation data introduced by peak intensity
  analysis and curve fitting methods.
\newblock {\em J. Biomol. NMR}, {\bf 21}, 1--9.
\newblock
  (\href{http://dx.doi.org/10.1023/A:1011966718826}{10.1023/A:1011966718826}).

\bibitem[Woessner, 1962]{Woessner62}
Woessner, D.~E. (1962).
\newblock Nuclear spin relaxation in ellipsoids undergoing rotational
  {B}rownian motion.
\newblock {\em J. Chem. Phys.}, {\bf 37}(3), 647--654.
\newblock (\href{http://dx.doi.org/10.1063/1.1701390}{10.1063/1.1701390}).

\bibitem[Zhuravleva et~al., 2004]{Zhuravleva04}
Zhuravleva, A.~V., Korzhnev, D.~M., Kupce, E., Arseniev, A.~S., Billeter, M.,
  and Orekhov, V.~Y. (2004).
\newblock Gated electron transfers and electron pathways in azurin: a {NMR}
  dynamic study at multiple fields and temperatures.
\newblock {\em J. Mol. Biol.}, {\bf 342}(5), 1599--1611.
\newblock
  (\href{http://dx.doi.org/10.1016/j.jmb.2004.08.001}{10.1016/j.jmb.2004.08.001}).

\bibitem[Zucchini, 2000]{Zucchini00}
Zucchini, W. (2000).
\newblock An introduction to model selection.
\newblock {\em J. Math. Psychol.}, {\bf 44}(1), 41--61.
\newblock
  (\href{http://dx.doi.org/10.1006/jmps.1999.1276}{10.1006/jmps.1999.1276}).

\end{thebibliography}
